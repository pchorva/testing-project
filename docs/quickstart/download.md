# Descargar PowerStudio

**PowerStudio Wave** es una aplicación dentro del paquete **PowerStudio** que permite gestionar la integración de equipos CIRCUTOR desde el navegador de una forma fàcil y amigable. Para empezar a utilizar **PowerStudio Wave** es necesario descargar e instalar el paquete de aplicaciones PowerStudio.

## Descarga

Puedes descargar la última versión de PowerStudio desde aquí.

[Descarga la última versión 23.0.0 :octicons-arrow-right-24:](https://circutor.com/){ .md-button .md-button--primary}

## Requisitos mínimos

* Procesador: **i5 de 10ª generación** (equivalente o superior)
* Memória RAM: **16 Gb**
* Almacenamiento: **2 Tb**
* Sistema operativo: **Windows Server 2016** (o superior) / **Windows 10 Pro** (o superior)
* **Adaptador Ethernet**

## Iniciar la instalación

Una vez descargado el software, deberás descomprimir el archivo ZIP e iniciar el asistente de instalación ejecutando el archivo comprimido.
    
```title="Nombre del archivo"
PowerStudio_Setup_v.**.**.*_x64_******.exe
```

!!! quote "Nota"

    La versión y la fecha están incluida en el nombre del archivo. Recomendamos comprobar que se va a instalar la última versión de PowerStudio. Puedes consultar la última versión en el [registro de cambios](/changelog/).