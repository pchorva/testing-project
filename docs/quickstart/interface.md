# Interfaz de usuario

## Introducción

La pantalla principal de PowerStudio Wave se divide en **cuatro** espacios: **Cabecera**, **Navegación**, **Contenido** y **Pie de página**.

![](../_custom/img/interface_screen.jpg)

## Cabecera

En la parte superior derecha de la cabecera se pueden encontrar las opciones de cambiar de idioma y de cerrar sesión. Además, puedes ver con el usuario que ha iniciado sesión.

![](../_custom/img/interface_header.jpg)

### Cambio de idioma

PowerStudio Wave está disponible actualmente en 3 idiomas: Español, Inglés y Catalán. Para cambiar el idioma, sólo es necesario abrir el desplegable y seleccionar el idioma deseado.

### Cerrar sesión

En la equina superior derecha, después del nombre de usuario que ha iniciado sesión, se encuentra el icono de **cerrar sesión**. Al hacer click en este icono, la sesión se cerrará y se irá automáticamente a la pantalla de **inicio de sesión**.

## Navegación

A la izquierda de la pantalla se encuentra la navegación por todos los módulos y funcionalidades de PowerStudio Wave. Cada una de ellas está detallada en los "Módulos" en esta documentación.

![](../_custom/img/interface_navigation.jpg)

A medida que se amplien funcionalidades de PowerStudio Wave, la navegación se ampliará y adaptará para facilitar el acceso a todas las funcionalidades de manera cómoda y ordenada.

## Pie de página

En la parte inferior derecha de la pantalla se encuentra el pie de página. En el pie de página se puede consultar el "Acerca de", activar la ayuda genérica y esconder/mostrar la navegación y la cabecera para disponer del contenido a pantalla completa.

![](../_custom/img/interface_footer.jpg)

### Pantalla completa

El icono de pantalla completa esconde/muestra la navegación y la cabecera para ganar más espacio para el contenido y poder trabajar de forma más cómoda.

Además, si se pulsa la tecla ++f11++, el navegador entra en pantalla completa y se puede disponer todavía de más espacio para el contenido.

### Ayuda

dsadasdas