# Aquí hemos escondido la navegación, solo en esta pagina.

## Sección de primer nivel.

| Syntax | Description |
| ---- | ----------- |
| Header | Title |
| Paragraph | Text |

[:octicons-download-24: Send](#){ .md-button .md-button--primary}

> Esyto es un blockquote con markdown estandar

[Esto es un link](https://www.example.com)

![alt text](_custom/logos/WaveDocs.svg)

*image_caption*

---

=== "desktop" 

    ![Image title](https://dummyimage.com/600x400/eee/aaa){ align=left width=300 }

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctoe massa, nec semper lorem quam in massa.

=== "mobile"

    contenido del mibolñe

## el gorod

Here is a simple footnote[^1]. With some additional text after it.

[^1]: My reference. El contenido de la nota al pie de dmo.

[Subscribe to our newsletter](#){ .md-button .md-button--primary }

!!! example "Phasellus posuere in sem ut cursus"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

??? note "Nota colapsable"

    Si añadimos un + después de los ??? de la nota, la nota colapsable saldrá expandida por defecto.
	
- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque


## 1. Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

### 2. Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

### Project layout fenced CODE by MD

```linenums="1"
mkdocs.yml    # The configuration file.
docs/
	index.md  # The documentation homepage.
	...       # Other markdown pages, images and other files.
```

### Fenced con titulo

```title="dasdasd"
mkdocs.yml    # The configuration file.
docs/
	index.md  # The documentation homepage.
	...       # Other markdown pages, images and other files.
```

### Tercer nivel

Lorem ipsum dolor sit amet, (1) consectetur adipiscing elit.
{ .annotate }

1.  I'm an annotation! I can contain `code`, __formatted
    text__, images, ... basically anything that can be expressed in Markdown.

#### ¿Hay cuarto nivel?

Esto está bajo un cuarto nivel.... vamos a probar 5 y 6?

Aprovecho este sitio para ==highlightear==

... inlcuso se pueden usar imagenes para determinar teclas: ++ctrl+"Z"++

##### Quinto nivel!!!

Revisar como se ve en TOC

###### Sexto? Ya seria exagerado...

!!! note "note"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! abstract "abstract"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! info "info"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! tip "tip"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! success "success"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! warning "warning"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! failure "failure"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! danger "danger"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! bug "bug"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! example "example"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	
!!! quote "quote"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.